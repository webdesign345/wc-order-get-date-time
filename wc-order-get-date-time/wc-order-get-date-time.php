<?php
/*
    Plugin Name: Woo Orders get date and time
    Plugin URI: #
    Description: This test plugin allows you to add the date and time of delivery of the selected product
    Author: Gistolovskiy Aleksandr
    Version: 1.0
    Author URI: https://disk.yandex.ru/i/4PlzUkGvDSoaIw
*/

/*  Copyright 2022 Gistolovskiy Aleksandr  (email: ale-wp345@yandex.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

defined( 'ABSPATH' ) || exit;

// DEFINE PATHS
define("ogdt_directory_url", plugin_dir_url(__FILE__));
define("wc_ogdt", "wc-order-get-date-time");
define("plugin_name", plugin_basename(__FILE__));
define("plugin_path", plugin_dir_path(__FILE__));

//Add default options
function wc_ogdt_default_data_save() {

    $Settings_Array = array(
            'wc_ogdt_title'                             => __('Desired delivery days and hours', wc_ogdt),
            'wc_ogdt_date_icon'                         => ogdt_directory_url . 'assets/front/images/icon_calendar.png',
            'wc_ogdt_days_limit_order_configuration'    => 2,
            'wc_ogdt_date_limit'                        => 15,
            'wc_ogdt_time_icon'                         => ogdt_directory_url . 'assets/front/images/icon_clock.png',
            'wc_ogdt_time_step'                         => 60,
            'wc_ogdt_time_1_start'                      => '10:00',
            'wc_ogdt_time_1_end'                        => '22:00',
            'wc_ogdt_time_2_start'                      => '11:00',
            'wc_ogdt_time_2_end'                        => '23:00',
        );

    add_option('wc_ogdt_default_settings', $Settings_Array);
}
register_activation_hook( __FILE__, 'wc_ogdt_default_data_save' );

// Remove all data when the plugin deleted
function wc_ogdt_default_data_delete() {

    $option_name = 'wc_ogdt_default_settings';

    delete_option($option_name);

    // for site options in Multisite
    if( is_multisite() ) delete_site_option($option_name);

}
register_uninstall_hook(__FILE__, 'wc_ogdt_default_data_delete');

// Add possibility for translate plugin to other language
function wc_ogdt_localize() {
    load_plugin_textdomain( wc_ogdt, false, dirname( plugin_basename(__FILE__)) . '/languages' );
}
add_action('plugins_loaded', 'wc_ogdt_localize');

/**
 * Plugin admin menu
 * Add subpage with plugin settings
 * Add block with shortcode interpreter
 * Add block with save settings
 */
require_once('inc/class-menu.php');
