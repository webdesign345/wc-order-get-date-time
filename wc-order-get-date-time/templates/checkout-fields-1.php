<?php
// Default template checkout additional fields

defined( 'ABSPATH' ) || exit;

//var_dump(WC());
$order_prepear = $wc_ogdt_option['wc_ogdt_days_limit_order_configuration'];
$date_stamp = mktime(0, 0, 0, date("m")  , ( date("d") + $order_prepear ), date("Y"));

?>
<div class="wc-ogdt-wrapper">

    <div class="wc-ogdt-title">
        <?php esc_html_e( $wc_ogdt_option['wc_ogdt_title'], wc_ogdt); ?>
    </div>

    <div class="wc-ogdt-container">
        <div class="wc-ogdt-date">
            <img class="wc-ogdt-date-icon" src="<?php esc_attr_e( esc_url( $wc_ogdt_option['wc_ogdt_date_icon'] ), wc_ogdt);?>" alt="">
            <input class="wc-ogdt-date-field" id="wc-ogdt-date-field" type="text" name="wc_ogdt_date_field" value="<?php esc_attr_e( date('d.m', $date_stamp) ); ?>" title="<?php esc_attr_e('Click to select a date', wc_ogdt);?>" required>
        </div>
        <div class="wc-ogdt-time">
            <img class="wc-ogdt-time-icon" src="<?php esc_attr_e( esc_url( $wc_ogdt_option['wc_ogdt_time_icon'] ), wc_ogdt);?>" alt="">
            <input class="wc-ogdt-time-start" type="text" name="wc_ogdt_time_start" title="<?php esc_attr_e('Click to select a start time', wc_ogdt);?>" required>
            <span class="wc-ogdt-time-divider">&mdash;</span>
            <input class="wc-ogdt-time-end" type="text" name="wc_ogdt_time_end" title="<?php esc_attr_e('Click to select a end time', wc_ogdt);?>" required>
        </div>
    </div>

</div>

<script type="text/javascript">
    (function($) {
        /*
        * jQuery UI Datepicker - https://api.jqueryui.com/datepicker/
        * jQuery timepicker - https://github.com/jonthornton/jquery-timepicker
        *
        * */

        const min_date = <?php echo $wc_ogdt_option['wc_ogdt_days_limit_order_configuration'];?>;
        const max_date = min_date + <?php echo $wc_ogdt_option['wc_ogdt_date_limit'];?>;

        // Initialize date jquery plugin
        $('#wc-ogdt-date-field').datepicker({
            firstDay: 1,
            dayNamesMin: [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
            monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
            dateFormat: "dd.mm",
            minDate: '"+' + min_date + 'D"',
            maxDate: '"+' + max_date + 'D"',
        });

        // Set default values for time before init time jquery plugin
        $('.wc-ogdt-time-start').val('<?php echo $wc_ogdt_option['wc_ogdt_time_1_start']; ?>');
        $('.wc-ogdt-time-end').val('<?php echo $wc_ogdt_option['wc_ogdt_time_2_start']; ?>');

        // Init first time field
        $('.wc-ogdt-time-start').timepicker({
                minTime: '<?php echo $wc_ogdt_option['wc_ogdt_time_1_start']; ?>',
                maxTime: '<?php echo $wc_ogdt_option['wc_ogdt_time_1_end']; ?>',
                step: <?php echo $wc_ogdt_option['wc_ogdt_time_step']; ?>,
                timeFormat: 'H:i',
            }
        );

        // Function for second init time field with condition using first field
        function initSecondTimepicker( min_time ) {
            let min_time_val = '';

            if(min_time) {
                min_time_val = min_time;
            } else {
                min_time_val = '<?php echo $wc_ogdt_option['wc_ogdt_time_2_start']; ?>'
            }

            $('.wc-ogdt-time-end').timepicker({
                minTime: min_time_val,
                maxTime: '<?php echo $wc_ogdt_option['wc_ogdt_time_2_end']; ?>',
                step: <?php echo $wc_ogdt_option['wc_ogdt_time_step']; ?>,
                timeFormat: 'H:i',
            });
        }

        // Default init for second time field
        initSecondTimepicker();

        // Listiner for first time field and update second init
        $('.wc-ogdt-time-start').on('changeTime', function() {
            const start_time = $(this).timepicker('getTime');
            const new_time = ( parseInt(start_time.getHours()) + 1 ) + ':00';

            $('.wc-ogdt-time-end').val(new_time);

            // Update init for second time field
            initSecondTimepicker(new_time);
        });
    })(jQuery);
</script>
