��          �      |      �     �     �          /     L  	   c     m     �     �     �     �     �     �  \   �     L     ]     p     �  	   �  '   �     �  �  �     �  -   �  F   �  H   )  -   r     �  -   �     �       4   %  /   Z  %   �     �  �   �  4   �	  .   �	  4   �	  .   
  &   I
  ?   p
     �
            
         	                                                                            Block title Click to select a date Click to select a end time Click to select a start time Date and time settings Date icon Date time settings Delivery day: Delivery time: Desired delivery days and hours Future date limit, in days Save Settings Settings The number of days to complete the order. A future date limit is added to the value, in days Time 1 end value Time 1 start value Time 2 end value Time 2 start value Time icon Time step, specify the value in minutes delivery time: Project-Id-Version: Разработка расширений для плагина Woocommerce
PO-Revision-Date: 2022-07-01 11:01+0300
Last-Translator: 
Language-Team: Гистоловский Александр
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_x;esc_attr_e;esc_html_e;esc_attr__;esc_html__
X-Poedit-SearchPath-0: inc
X-Poedit-SearchPath-1: templates
X-Poedit-SearchPath-2: wc-order-get-date-time.php
X-Poedit-SearchPathExcluded-0: assets
 Заголовок блока Кликните для выбора даты Кликните для выбора конечного времени Кликните для выбора начального времени Настройки даты и времени Иконка поля даты Настройки даты и времени День доставки: Время доставки: Желаемые дни и часы доставки Лимит будущей даты, в днях Сохранить настройки Настройки Количество дней для сбора заказа. Данный параметр будет добавлен к значению лимита будущей даты, в днях Значение окончания времени 1 Значение начала времени 1 Значение окончания времени 2 Значение начала времени 2 Иконка полей времени Шаг времени, указывается в минутах время доставки: 