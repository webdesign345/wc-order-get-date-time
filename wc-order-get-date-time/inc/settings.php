<?php

defined( 'ABSPATH' ) || exit;

$wc_ogdt_option = get_option('wc_ogdt_default_settings');

$wc_ogdt_title = ( $_POST['wc_ogdt_title'] ) ? sanitize_text_field( $_POST['wc_ogdt_title'] ) : $wc_ogdt_option['wc_ogdt_title'];
$wc_ogdt_date_icon = ( $_POST['wc_ogdt_date_icon'] ) ? sanitize_text_field( $_POST['wc_ogdt_date_icon'] ) : $wc_ogdt_option['wc_ogdt_date_icon'];
$wc_ogdt_days_limit_order_configuration = ( $_POST['wc_ogdt_days_limit_order_configuration'] ) ? (int) sanitize_text_field( $_POST['wc_ogdt_days_limit_order_configuration'] ) : $wc_ogdt_option['wc_ogdt_days_limit_order_configuration'];
$wc_ogdt_date_limit = ( $_POST['wc_ogdt_date_limit'] ) ? (int) sanitize_text_field( $_POST['wc_ogdt_date_limit'] ) : $wc_ogdt_option['wc_ogdt_date_limit'];
$wc_ogdt_time_icon = ( $_POST['wc_ogdt_time_icon'] ) ? sanitize_text_field( $_POST['wc_ogdt_time_icon'] ) : $wc_ogdt_option['wc_ogdt_time_icon'];
$wc_ogdt_time_step = ( $_POST['wc_ogdt_time_step'] ) ? (int) sanitize_text_field( $_POST['wc_ogdt_time_step'] ) : $wc_ogdt_option['wc_ogdt_time_step'];

$wc_ogdt_time_1_start = ( $_POST['wc_ogdt_time_1_start'] ) ? sanitize_text_field( $_POST['wc_ogdt_time_1_start'] ) : $wc_ogdt_option['wc_ogdt_time_1_start'];
$wc_ogdt_time_1_end = ( $_POST['wc_ogdt_time_1_end'] ) ? sanitize_text_field( $_POST['wc_ogdt_time_1_end'] ) : $wc_ogdt_option['wc_ogdt_time_1_end'];

$wc_ogdt_time_2_start = ( $_POST['wc_ogdt_time_2_start'] ) ? sanitize_text_field( $_POST['wc_ogdt_time_2_start'] ) : $wc_ogdt_option['wc_ogdt_time_2_start'];
$wc_ogdt_time_2_end = ( $_POST['wc_ogdt_time_2_end'] ) ? sanitize_text_field( $_POST['wc_ogdt_time_2_end'] ) : $wc_ogdt_option['wc_ogdt_time_2_end'];

if( $_POST['wc_ogdt_title'] ) {
    $wc_ogdt_settings = array(
        'wc_ogdt_title' =>  $wc_ogdt_title,
        'wc_ogdt_date_icon' =>  $wc_ogdt_date_icon,
        'wc_ogdt_days_limit_order_configuration' =>  $wc_ogdt_days_limit_order_configuration,
        'wc_ogdt_date_limit' =>  $wc_ogdt_date_limit,
        'wc_ogdt_time_icon' =>  $wc_ogdt_time_icon,
        'wc_ogdt_time_step' =>  $wc_ogdt_time_step,
        'wc_ogdt_time_1_start' =>  $wc_ogdt_time_1_start,
        'wc_ogdt_time_1_end' =>  $wc_ogdt_time_1_end,
        'wc_ogdt_time_2_start' =>  $wc_ogdt_time_2_start,
        'wc_ogdt_time_2_end' =>  $wc_ogdt_time_2_end,
    );

    update_option('wc_ogdt_default_settings', $wc_ogdt_settings);
}

do_action('wc_ogdt_before_form', $wc_ogdt_settings);

?>
<form class="wc_ogdt_form" action="" method="post">

    <label for="wc_ogdt_form-title">
        <?php _e('Block title', wc_ogdt); ?>
        <input class="wc_ogdt_form-date-title" id="wc_ogdt_form-date-title" name="wc_ogdt_title" type="text" value="<?php esc_attr_e($wc_ogdt_title); ?>" required>
    </label>

    <label for="wc_ogdt_form-date-icon">
        <?php _e('Date icon', wc_ogdt); ?>
        <img height="30" width="30" src="<?php echo esc_url($wc_ogdt_date_icon); ?>" alt="">
        <input class="wc_ogdt_form-date-icon" id="wc_ogdt_form-date-icon" name="wc_ogdt_date_icon" type="text" value="<?php echo esc_url($wc_ogdt_date_icon); ?>">
    </label>

    <label for="wc_ogdt_form-days_limit_order_configuration">
        <?php _e('The number of days to complete the order. A future date limit is added to the value, in days', wc_ogdt); ?>
        <input class="wc_ogdt_form-days-limit-order-configuration" id="wc_ogdt_form-days-limit-order-configuration" name="wc_ogdt_days_limit_order_configuration" type="number" value="<?php esc_attr_e($wc_ogdt_days_limit_order_configuration); ?>" required>
    </label>

    <label for="wc_ogdt_form-date-limit">
        <?php _e('Future date limit, in days', wc_ogdt); ?>
        <input class="wc_ogdt_form-date-limit" id="wc_ogdt_form-date-limit" name="wc_ogdt_date_limit" type="number" value="<?php esc_attr_e($wc_ogdt_date_limit); ?>" required>
    </label>

    <label for="wc_ogdt_form-time-icon">
        <?php _e('Time icon', wc_ogdt); ?>
        <img height="30" width="30" src="<?php echo esc_url($wc_ogdt_time_icon); ?>" alt="">
        <input class="wc_ogdt_form-time-icon" id="wc_ogdt_form-time-icon" name="wc_ogdt_time_icon" type="text" value="<?php echo esc_url($wc_ogdt_time_icon); ?>">
    </label>

    <label for="wc_ogdt_form-time-step">
        <?php _e('Time step, specify the value in minutes', wc_ogdt); ?>
        <input class="wc_ogdt_form-time-step" id="wc_ogdt_form-time-step" name="wc_ogdt_time_step" type="number" value="<?php esc_attr_e($wc_ogdt_time_step); ?>" required>
    </label>

    <label for="wc_ogdt_form-time-1-start">
        <?php _e('Time 1 start value', wc_ogdt); ?>
        <input class="wc_ogdt_form-time-1-start" id="wc_ogdt_form-time-1-start" name="wc_ogdt_time_1_start" type="text" value="<?php esc_attr_e($wc_ogdt_time_1_start); ?>" required>
    </label>

    <label for="wc_ogdt_form-time-1-end">
        <?php _e('Time 1 end value', wc_ogdt); ?>
        <input class="wc_ogdt_form-time-1-end" id="wc_ogdt_form-time-1-end" name="wc_ogdt_time_1_end" type="text" value="<?php esc_attr_e($wc_ogdt_time_1_end); ?>" required>
    </label>

    <label for="wc_ogdt_form-time-2-start">
        <?php _e('Time 2 start value', wc_ogdt); ?>
        <input class="wc_ogdt_form-time-2-start" id="wc_ogdt_form-time-2-start" name="wc_ogdt_time_2_start" type="text" value="<?php esc_attr_e($wc_ogdt_time_2_start); ?>" required>
    </label>

    <label for="wc_ogdt_form-time-2-end">
        <?php _e('Time 2 end value', wc_ogdt); ?>
        <input class="wc_ogdt_form-time-2-end" id="wc_ogdt_form-time-2-end" name="wc_ogdt_time_2_end" type="text" value="<?php esc_attr_e($wc_ogdt_time_2_end); ?>" required>
    </label>

    <input type="submit" value="<?php esc_attr_e('Save Settings', wc_ogdt);?>">
</form>

<?php

do_action('wc_ogdt_after_form', $wc_ogdt_settings);

?>
