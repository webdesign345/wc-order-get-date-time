<?php

defined( 'ABSPATH' ) || exit;

class WC_OGDT_Menu {

    // Initialize main plugin functional
    public function __construct() {

        // Additional functionality for displaying the date and time on the default checkout page
        add_shortcode('wc_order_get_dt', array( $this, 'fn_wc_order_get_dt'));

        // The functionality of displaying the date and time on the checkout page by default
        add_action('woocommerce_checkout_after_order_review', array( $this, 'ogdt_function'), 10);

        // Add styles and scripts for admin page
        add_action('admin_enqueue_scripts', array( $this, 'wc_ogdt_admin_scripts'));

        // Add styles and scripts for front page
        add_action('wp_enqueue_scripts', array( $this, 'wc_ogdt_front_scripts'));

        if (is_admin()) {
            add_action('admin_menu', array( $this, 'wc_ogdt_submenu_page'), 100);
        }

        // Add settings link on plugins page
        $plugin = plugin_name;
        add_filter("plugin_action_links_$plugin", array( $this, 'wc_ogdt_settings_link') );

        // Add custom data to post_data and session
        add_filter('woocommerce_checkout_posted_data', array( $this, 'wc_ogdt_checkout_posted_data'), 20);

        // Add custom data to order meta
        add_action('woocommerce_checkout_update_order_meta', array( $this, 'wc_ogdt_woocommerce_checkout_update_order_meta' ), 20, 2 );

        // Add custom data (day and time) to order in admin area
        add_action('woocommerce_admin_order_data_after_billing_address', array( $this, 'wc_ogdt_woocommerce_admin_order_data_after_billing_address'), 20);

        // Add custom data(day and time) to email
        add_filter( 'woocommerce_mail_content', array( $this, 'wc_ogdt_woocommerce_mail_content'), 20);

    }

    // Additional functionality for displaying the date and time on the default checkout page
    public function fn_wc_order_get_dt() {

        $wc_ogdt_option =  get_option('wc_ogdt_default_settings');

        do_action('before_wc_order_get_dt', $wc_ogdt_option);

        ob_start();

        require_once(plugin_path . '/templates/checkout-fields-1.php');

        $output_template = ob_get_contents();
        ob_clean();

        do_action('after_wc_order_get_dt', $output_template);

        return $output_template;

    }

    // The functionality of displaying the date and time on the checkout page by default
    public function ogdt_function() {
        echo apply_filters('output_data_wc_order_get_dt', do_shortcode('[wc_order_get_dt]') );
    }

    // Add styles and scripts for admin page
    public function wc_ogdt_admin_scripts(){
            wp_enqueue_style('wc-ogdt-admin-style', ogdt_directory_url .'assets/admin/css/wc_ogdt_admin_styles.css');
    }

    // Add styles and scripts for front page
    public function wc_ogdt_front_scripts() {
        if(is_checkout()) {
            wp_enqueue_style('jquery-ui', ogdt_directory_url . 'assets/front/css/jquery-ui.min.css' );
            wp_enqueue_style('jquery-timepicker', ogdt_directory_url . 'assets/front/css/jquery.timepicker.min.css' );

            wp_enqueue_style( 'wc-ogdt-styles', ogdt_directory_url . 'assets/front/css/wc_ogdt_styles.css');


            // Add ajax file
            wp_enqueue_script('jquery');
            wp_enqueue_script( 'jquery-ui', ogdt_directory_url . 'assets/front/js/jquery-ui.min.js', array( 'jquery-core' ), false );
            wp_enqueue_script( 'jquery-timepicker', ogdt_directory_url . 'assets/front/js/jquery.timepicker.min.js', array( 'jquery-core' ), false );
        }
    }

    // Set position for setting block on same page
    public function wc_ogdt_submenu_page(){
        add_submenu_page( 'woocommerce', __('Date and time settings', wc_ogdt), __('Date time settings', wc_ogdt), 'manage_options', 'wc-ogdt-settings', array( $this, 'wc_ogdt_menu_page'), 10 );
    }

    // Main plugin setting and save page
    public function wc_ogdt_menu_page(){
        require_once('settings.php');
    }

    // Add settings link on plugins page
    public function wc_ogdt_settings_link($links = null) {

        $settings_link = '<a href="admin.php?page=wc-ogdt-settings">' . __("Settings", wc_ogdt) . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }

    // Add custom data to post_data and session
    public function wc_ogdt_checkout_posted_data( $post_data ) {

        do_action('wc_ogdt_before_post_data', $post_data);

        $post_data['wc_ogdt_date_field'] = ( $_POST['wc_ogdt_date_field'] ) ? sanitize_text_field( $_POST['wc_ogdt_date_field'] ) : '';
        $post_data['wc_ogdt_time_start'] = ( $_POST['wc_ogdt_time_start'] ) ? sanitize_text_field( $_POST['wc_ogdt_time_start'] ) : '';
        $post_data['wc_ogdt_time_end'] = ( $_POST['wc_ogdt_time_end'] ) ? sanitize_text_field( $_POST['wc_ogdt_time_end'] ) : '';

        WC()->session->set( 'wc_ogdt_date_field', $post_data['wc_ogdt_date_field'] );
        WC()->session->set( 'wc_ogdt_time_start', $post_data['wc_ogdt_time_start'] );
        WC()->session->set( 'wc_ogdt_time_end', $post_data['wc_ogdt_time_end'] );

        do_action('wc_ogdt_after_post_data', $post_data, WC()->session);

        return $post_data;
    }

    // Add custom data to order meta
    public function wc_ogdt_woocommerce_checkout_update_order_meta( $order_id, $data ) {

        do_action('wc_ogdt_before_update_meta', $order_id, $data);

        update_post_meta( $order_id, '_wc_ogdt_date_field', $data['wc_ogdt_date_field'] );
        update_post_meta( $order_id, '_wc_ogdt_time_start', $data['wc_ogdt_time_start'] );
        update_post_meta( $order_id, '_wc_ogdt_time_end', $data['wc_ogdt_time_end'] );

        do_action('wc_ogdt_after_update_meta', $order_id, $data);
    }

    // Add custom data (day and time) to order in admin area
    public function wc_ogdt_woocommerce_admin_order_data_after_billing_address( $order ) {

        /*
         * -  _wc_ogdt_date_field
         * -  _wc_ogdt_time_start
         * -  _wc_ogdt_time_end
         */

        do_action('wc_ogdt_before_output_in_admin_order_page', $order );

        $wc_ogdt_date_field = get_post_meta($order->id, '_wc_ogdt_date_field', true);
        $wc_ogdt_time_start = get_post_meta($order->id, '_wc_ogdt_time_start', true);
        $wc_ogdt_time_end = get_post_meta($order->id,  '_wc_ogdt_time_end', true);

        echo apply_filters( 'wc_ogdt_output_in_admin_order_page', sprintf('<h3>%s %s, %s %s</h3>',
            __('Delivery day:', wc_ogdt),
            $wc_ogdt_date_field,
            __('delivery time:', wc_ogdt),
            $wc_ogdt_time_start . '&mdash;' . $wc_ogdt_time_end
        ));
    }

    // Add custom data(day and time) to email
    public function wc_ogdt_woocommerce_mail_content( $content ){

        $wc_ogdt_date_field = WC()->session->get('wc_ogdt_date_field');
        $wc_ogdt_time_start = WC()->session->get('wc_ogdt_time_start');
        $wc_ogdt_time_end = WC()->session->get('wc_ogdt_time_end');
        $str = sprintf('<span>%s %s, %s %s</span>',
            __('Delivery day:', wc_ogdt),
            $wc_ogdt_date_field,
            __('Delivery time:', wc_ogdt),
            $wc_ogdt_time_start . '&mdash;' .  $wc_ogdt_time_end
        );

        $content = str_replace(')</h2>', ')</h2>' . $str, $content);

        return apply_filters('wc_ogdt_insert_custom_data_to_mail', $content );
    }

}

global $wc_ogdt_menu;
$wc_ogdt_menu = new WC_OGDT_Menu();

